# Wireguard  
Written 20-08-2021  
Updated 24-09-2021

Wireguard is a modern VPN protocol that aims to be secure, fast, and easy to use. It is cross platform (Linux, OpenBSD, Windows, MacOS, Android, iOS), easy to deploy, has seamless switch between networks, and is not resource hungry, making it also good for embedded or IoT devices.  

See official website at: https://wireguard.com/

## Reference  
Please refer to the official documentation in case of issues, or if this guide is out of date:    
https://www.wireguard.com/install/  
https://www.wireguard.com/quickstart/  

## Manual setup:  
Setting up a wireguard connection requires at least two devices: a server and a client. Both devices have a keypair, one public key and one private key. The devices are required to know each others public keys and have wireguard installed.   

In my setup, I have an OpenBSD device as a wireguard server, and a Linux device as a wireguard client.  
(There is also guide for a Linux device as a wireguard server, and a Windows device as a wireguard client below)  

Here are two example configurations:

Server (OpenBSD /etc/hostname.wg0):
```
inet 10.0.0.1 255.255.255.0
wgkey SERVERPRIVATEKEY
wgport 16754
wgpeer CLIENTPUBLICKEY wgaip 10.0.0.2/32
up

```

Client:
```
[Interface]
PrivateKey = CLIENTPRIVATEKEY
Address = 10.0.0.2/32
DNS = 8.8.8.8

[Peer]
PublicKey = SERVERPUBLICKEY
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = 104.236.30.24:16754
```

### Server - OpenBSD
Wireguard is included in the default installation since OpenBSD 6.8 and does not require additional installation.  

- Generate private key (Remember not to share this with anyone!)
```
openssl rand -base64 32

```
- Create a new file at /etc/hostname.wg0 where wg0 is the virtual interface name:    

inet - IP address for the Wireguard interface and subnet mask for the address range of the VPN network  
wgkey - Wireguard private key generated in the previous step using openssl  
wgport - The port wireguard will be running on  
wgpeer - Wireguard public key of the client  - wgaip - IP address that is allowed to enter the VPN tunnel from the clients side 
up - Bring the network interface up  

There can be multiple peers set up. An example of /etc/hostname.wg0:  
```
inet 10.0.0.1 255.255.255.0
wgkey ue5fFiqPvA8wX3PvDgSs9we/6o6u6lPQWKB94FEbzlY=
wgport 16754
wgpeer hmS3VFVHInuKOY23Ykrgvni2tzynxICL12e1lamtdlk= wgaip 10.0.0.2/32
wgpeer SECONDCLIENTPUBLICKEY wgaip 10.0.0.3/32
wgpeer THIRDCLIENTPUBLICKEY wgaip 10.0.0.4/32
up
```
This configuration means the OpenBSD will have an interface named wg0 that is for the Wireguard server where it will have the address of 10.0.0.1.
The connecting clients are called peers, and the IP address defined in wgaip limits the incoming traffic from that IP only.

- Change permissions of the file:  
```
chmod 640 /etc/hostname.wg0
chown root:wheel /etc/hostname.wg0
```
- Enable packet forwarding between interfaces by adding the following to /etc/sysctl.conf:  

```
net.inet.ip.forwarding=1
```
-  Allow Wireguard clients to access the Internet on the PF firewall by adding the following to /etc/pf.conf right below the first "pass":  
```
match out on egress from wg0:network to any nat-to egress
```
"egress" includes all interfaces

- Reset the networking interfaces using
```
sh /etc/netstart wg0
```

Running "ifconfig wg0" will show you wgpubkey which is the public key of your server. You need this public key later to enter into your client.  

For more information run the "man wg" command.  

### Server - Linux (Debian)
into /etc/sysctl.conf
```
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding=1
```
sysctl -p

Into /etc/wireguard/wg0.conf

```
[Interface]
PrivateKey = ue5fFiqPvA8wX3PvDgSs9we/6o6u6lPQWKB94FEbzlY=
ListenPort = 16754
Address = 10.0.0.1/32
PostUp = ufw route allow in on wg0 out on eth0
PostUp = iptables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
PostUp = ip6tables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
PreDown = ufw route delete allow in on wg0 out on eth0
PreDown = iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
PreDown = ip6tables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

[Peer]
PublicKey = hmS3VFVHInuKOY23Ykrgvni2tzynxICL12e1lamtdlk=
AllowedIPs = 10.0.0.2/32
```
To start and stop
wg-quick up wg0
wg-quick down wg0

To autostart
```
systemctl enable wg-quick@wg0
```

### Client (GUI) - Windows, MacOS, Android, iOS, Ubuntu  
- Download links: https://www.wireguard.com/install/  
  Windows: Install Wireguard from [the official website](https://download.wireguard.com/windows-client/wireguard-installer.exe).  
  MacOS: Install Wireguard from the [App Store](https://itunes.apple.com/us/app/wireguard/id1451685025?ls=1&mt=12).   
  Android: Download from [Google Play Store](https://play.google.com/store/apps/details?id=com.wireguard.android) or [F-Droid](https://f-droid.org/en/packages/com.wireguard.android/).  
  iOS: Install from [App Store](https://itunes.apple.com/us/app/wireguard/id1441195209?ls=1&mt=8).  
  Ubuntu: Install using the terminal: apt install wireguard  

- Create a new Wireguard tunnel (Click to “Add Tunnel” -> “Add empty tunnel” -> “Create new tunnel”):

You will also see the public key of your Wireguard client, you need this for the server!   

[Interface]
Privatekey: Client's Wireguard private key (Do not share with anyone!)  
Address: IP address assigned to the client on the server    
DNS: DNS server the client should use  

[Peer]
PublicKey: Servers Wireguard public key  
AllowedIPs: Acts sort of like a routing table    
Endpoint: IP address and port of the Wireguard server (should be a public IP address)  

Here is an example configuration:

```
[Interface]
PrivateKey = CLIENTPRIVATEKEY
Address = 10.0.0.2/32
DNS = 8.8.8.8

[Peer]
PublicKey = SERVERPUBLICKEY
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = 104.236.30.24:16754
```

Additionally on Windows if behind NAT, it might be necessary to add the following line under "Endpoint":  
```
PersistentKeepalive = 25
```


### Client (CLI) - Linux (Gentoo)  

- Generate Wireguard Public and Private keys using  
```
$(umask 077; wg genkey | tee privatekey | wg pubkey > publickey)  
```
- Create a Wireguard configuration in /etc/wireguard/wg0.conf where wg0 is the configuration and interface name:  

[Interface]
Privatekey: Client's Wireguard private key (Do not share with anyone!)  
Address: IP address assigned to the client on the server    
DNS: DNS server the client should use  

[Peer]
PublicKey: Servers Wireguard public key  
AllowedIPs: Acts sort of like a routing table  
Endpoint: IP address and port of the Wireguard server (should be a public IP address)  

Here is an example configuration:

```
[Interface]
PrivateKey = CLIENTPRIVATEKEY
Address = 10.0.0.2/32
DNS = 8.8.8.8

[Peer]
PublicKey = SERVERPUBLICKEY
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = 104.236.30.24:16754
```
- Connect using
```
wg-quick up wg0
```
- Disconnect using
```
wg-quick down wg0
```
- If using iptables, remember to create rules
```
iptables -A FORWARD 1 -o wg+ -j ACCEPT
iptables -A FORWARD 1 -i wg+ -j ACCEPT
```
