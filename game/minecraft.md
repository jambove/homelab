# Minecraft   
Written 20-08-2021  

Minecraft is a sandbox video game that has two editions Java and Bedrock. This guide will describe setting up a Minecraft Java server, and operating it using tmux the terminal multiplexer. The provided script is mainly a wrapper for tmux commands with some additional functions. The goal of the script is to provide a simple to use and lean solution for Minecraft server administrators.    

See official website at: https://minecraft.net/      


## Installation:
All the commands below are ran on Debian 11 and requires root privileges.    
```
apt update
apt install default-jdk tmux
```

Optional but recommended: Set up firewall (this guide uses ufw)  
```
apt install ufw
ufw allow 25565/tcp # Default Minecraft server port is 25565
ufw enable # Enable UFW firewall
```
- Port forward if behind NAT, in this case: Yes    

## Choosing a Server jar  
- The official stable server files can be downloaded from https://www.minecraft.net/en-us/download/server    
- The nightly snapshot files are linked at the bottom of each individual version for example: https://www.minecraft.net/en-us/article/minecraft-snapshot-21w13a    
- Spigot is a version that has Bukkit and allows larger mods to be added. Download from https://www.spigotmc.org    
- Paper is a version that is optimized, and is recommended if the server does not have much resources. Download from https://papermc.io/downloads    
This guide uses the Paper version. But in the end it does not matter as long as you have a server.jar.  

## Setup and first run  

- Download a server jar from the previous section.  
- Create a folder for your minecraft server (minecraftserver), and move the server.jar (exact names) inside.  
- Download the script from [here](scripts/minecraft) and place into .local/bin, give executive permissions.   
- Start the server with  
```
minecraft start
```
- It will shut down after generating a few files.  
- Edit the eula.txt and change false to true to accept the EULA.  
- Server.properties is for configuring the server's settings.  
- Start the server  

## Server operation

- To print out help run
```
minecraft help
```

Available options:    

- minecraft start - starts the server  
- minecraft stop - stops the server  
- minecraft restart - stops the server, waits 15 seconds, and starts the server  
- minecraft attach - Attaches the server console: can use minecraft server commands such as "say hello". To detach without stopping, use CTRL+b, then d   
