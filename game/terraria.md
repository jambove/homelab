# Terraria   
Written 20-08-2021  

Terraria is a sandbox video game. This guide will describe setting up a Terraria server, and operating it using tmux the terminal multiplexer. The provided script is mainly a wrapper for tmux commands with some additional functions. The goal of the script is to provide a simple to use and lean solution for Terraria server administrators.    

See official website at: https://terraria.org/


## Installation:
All the commands below are ran on Debian 11 and requires root privileges.    
```
apt update
apt install tmux
```

Optional but recommended: Set up firewall (this guide uses ufw)  
```
apt install ufw
ufw allow 7777/tcp # Default Terraria server port is 7777
ufw enable # Enable UFW firewall
```
- Port forward if behind NAT, in this case: Yes    

## Setup and first run  

- Download a server zip from https://terraria.org/api/download/pc-dedicated-server/terraria-server-1423.zip. 
- Create a folder for your terraria server (terraria) and move the server zip inside.  
- Download the script from [here](scripts/terraria) and place into .local/bin, give executive permissions.   
- Start the server with  
```
terraria start
```
- serverconfig.txt is for configuring the server's settings.  
 
## Server operation

- To print out help run
```
terraria help
```

Available options:    

- terraria start - starts the server  
- terraria stop - stops the server  
- terraria restart - stops the server, waits 15 seconds, and starts the server  
- terraria attach - Attaches the server console: can use terraria server commands. To detach without stopping, use CTRL+b, then d   
