# Homelab

Repository for my personal homelab/systems.

## Overview  
This project is a collection of machines - most of them which are virtualized, used as different types of servers for the purpose of experimentation and providing services.

## Purpose  
The aim of this project is to create several virtual devices and networks with different operating systems to emulate real world scenarios for better understanding how similar systems are set up from scratch and how they work. The machines may also provide services useful for several users. It is also for fun.  

## Audience  
The audience is anyone who is interested in IT, mainly interested in system administration, networking, unix systems, and virtualization. It could also be anyone who is just curious about what I work on, or how I do tasks.     

## Systems 
<table>
	<tr>
		<th>OS</th>
		<th>Description</th>
		<th>Info</th>
	</tr>
	<tr>
		<td>Debian</td>
		<td>Server for: Jitsi, Searx, Discord bot</td>
		<td>2 CPU, 4GB RAM</td>
	</tr>
	<tr>
		<td>OpenBSD</td>
		<td>Wireguard VPN server</td>
		<td>1 CPU, 2GB RAM</td>
	</tr>
	<tr>
		<td>Gentoo</td>
		<td>Test box, dev environment for my laptop</td>
		<td>3 CPU, 8GB RAM</td>
	</tr>
	<tr>
		<td>Arch</td>
		<td>Rolling-release testing box</td>
		<td>1 CPU, 2GB RAM</td>
	</tr>
	<tr>
		<td>Debian</td>
		<td>Game server: Minecraft, Terraria, Don't Starve Together</td>
		<td>2 CPU, 8GB RAM</td>
	</tr>
</table>

Most of these machines are/were on a server running Proxmox as a Hypervisor     
Hardware: Intel NUC 12 CPU(2 thread/6 core), 64 GB RAM, 1 TB SSD  

## IP addresses  
Currently for security purposes the network details/addresses are not specified.  

## High Level Diagram  

![](netdiagram.png)



