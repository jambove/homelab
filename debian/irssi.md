# Irssi  
Written 04-10-2021  

Irssi is an terminal based IRC client.

See official website at: https://irssi.org/

## Reference  
Please refer to the official documentation in case of issues, or if this guide is out of date:    
https://irssi.org/documentation/

## Setup
- Install using
```
apt install irssi
```
- Installing tmux or screen may be a good idea if you want to stay connected.
```
apt install tmux
```
- Run "tmux" and then run "irssi" or run
```
tmux new -d -s irc 'irssi'
```
- Attach using
```
tmux a -t irc
```

- Configuration commands can be run within irssi and saved using /save or configuration can be changed in ~/.irssi/config
- Servers can be added using 
```
servers = (
  {
    address = "irc.baconsvin.org";
    chatnet = "bornhack";
    port = "6697";
    use_tls = "yes";
    tls_verify = "yes";
    autoconnect = "yes";
  },
  {
    address = "irc.libera.chat";
    chatnet = "libera";
    port = "6697";
    use_tls = "yes";
    tls_verify = "yes";
    autoconnect = "yes";
  }
);
```
- To set up autoconnect, user and/or password has to be added
- Note: In this case your password is plaintext in your config file which is not secure in case of a malicious root user even if you chmod 700 ~/.irssi  
- You may omit the password and log in after every restart from inside the client
```
chatnets = {
  bornhack = { 
    type = "IRC"; 
    sasl_mechanism = "PLAIN";
    sasl_username = "jambove";
    sasl_password = "changeme";
};
  libera = {
    type = "IRC";
    sasl_mechanism = "PLAIN";
    sasl_username = "jambove";
    sasl_password = "changeme";
  };
};

```
- Additionally, autoconnect channels can be added with
```
channels = (
  { name = "#bornhack"; chatnet = "bornhack"; autojoin = "yes"; },
  { name = "#gentoo"; chatnet = "libera"; autojoin = "yes"; },
  { name = "#openbsd"; chatnet = "libera"; autojoin = "yes"; }
);
```
- Default user settings can be changed under
```
settings = {
  core = { 
    real_name = "Changeme";
    user_name = "jambove";
    nick = "jambove";
  };
  "fe-text" = { actlist_sort = "refnum"; };
};
```
- On bigger channels join,quit,parts,and nicks messages may disrupt the chat, they can be ignored using
```
ignores = (
  { level = "JOINS PARTS QUITS NICKS"; channels = ( "#openbsd" ); },
  { level = "JOINS PARTS QUITS NICKS"; channels = ( "#gentoo" ); }
);
```

- To run an irssi instance in a tmux after reboot, add the following to crontab using the 'crontab -e' command
```
@reboot tmux new -d -s irc 'irssi'
```




