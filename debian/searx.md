# Searx  
Written 25-04-2021  

Searx is a is an internet metasearch engine which gathers search results from other search services. Searches are not tracked, profiled, or logged(if configured so). It is libre software and can be self-hosted. I prefer using it not as a drop-in replacement for all searches - but as an addition to more obscure searches as it aggregates results from other engines.  

See official website at: https://searx.me/ and use public instances at https://searx.space/    

## Reference  
Please refer to the official documentation in case of issues, or if this guide is out of date:  
https://searx.github.io/searx/admin/installation-searx.html
https://searx.github.io/searx/admin/installation-uwsgi.html#searx-uwsgi

## Manual installation
All the commands below are ran on Debian 10 and requires root privileges  
```
apt install python3-dev python3-pip python3-babel python3-venv uwsgi uwsgi-plugin-python3 git build-essential libxslt-dev zlib1g-dev libffi-dev libssl-dev shellcheck
useradd --shell /bin/bash --system --home-dir "/usr/local/searx" --comment 'Privacy-respecting metasearch engine' searx
mkdir /usr/local/searx
chown -R searx:searx /usr/local/searx
su - searx << EOF
git clone "https://github.com/searx/searx.git" "/usr/local/searx/searx-src"
python3 -m venv "/usr/local/searx/searx-pyenv"
echo ". /usr/local/searx/searx-pyenv/bin/activate" >>  "/usr/local/searx/.profile"
EOF
su - searx << EOF
pip3 install -U pip
pip3 install -U setuptools
pip3 install -U wheel
pip3 install -U pyyaml
cd "/usr/local/searx/searx-src"
pip3 install -e .
EOF
mkdir -p /etc/searx
cp /usr/local/searx/searx-src/searx/settings.yml /etc/searx/settings.yml
sed -i -e "s/ultrasecretkey/$(openssl rand -hex 16)/g" "/etc/searx/settings.yml"
sed -i -e "s/{instance_name}/searx@$(uname -n)/g" "/etc/searx/settings.yml"
```
- Use wanted settings at /etc/searx/settings.yml (bind_address should not be 127.0.0.1)
- Enable uwsgi for running searx automatically
```
touch /etc/uwsgi/apps-available/searx.ini
ln -s /etc/uwsgi/apps-available/searx.ini /etc/uwsgi/apps-enabled/
```
- Add the following (tailored to needs) to /etc/uwsgi/apps-available/searx.ini :  
```
[uwsgi]

# uWSGI core
# ----------
#
# https://uwsgi-docs.readthedocs.io/en/latest/Options.html#uwsgi-core

# Who will run the code
uid = searx
gid = searx

# set (python) default encoding UTF-8
env = LANG=C.UTF-8
env = LANGUAGE=C.UTF-8
env = LC_ALL=C.UTF-8

# chdir to specified directory before apps loading
chdir = /usr/local/searx/searx-src/searx

# searx configuration (settings.yml)
env = SEARX_SETTINGS_PATH=/etc/searx/settings.yml

# disable logging for privacy
disable-logging = true

# The right granted on the created socket
chmod-socket = 666

# Plugin to use and interpretor config
single-interpreter = true

# enable master process
master = true

# load apps in each worker instead of the master
lazy-apps = true

# load uWSGI plugins
plugin = python3,http

# By default the Python plugin does not initialize the GIL.  This means your
# app-generated threads will not run.  If you need threads, remember to enable
# them with enable-threads.  Running uWSGI in multithreading mode (with the
# threads options) will automatically enable threading support. This *strange*
# default behaviour is for performance reasons.
enable-threads = true


# plugin: python
# --------------
#
# https://uwsgi-docs.readthedocs.io/en/latest/Options.html#plugin-python

# load a WSGI module
module = searx.webapp

# set PYTHONHOME/virtualenv
virtualenv = /usr/local/searx/searx-pyenv

# add directory (or glob) to pythonpath
pythonpath = /usr/local/searx/searx-src


# speak to upstream
# -----------------
#
# Activate the 'http' configuration for filtron or activate the 'socket'
# configuration if you setup your HTTP server to use uWSGI protocol via sockets.

# using IP:
#
# https://uwsgi-docs.readthedocs.io/en/latest/Options.html#plugin-http
# Native HTTP support: https://uwsgi-docs.readthedocs.io/en/latest/HTTP.html

http = 127.0.0.1:8888

# using unix-sockets:
#
# On some distributions you need to create the app folder for the sockets::
#
#   mkdir -p /run/uwsgi/app/searx
#   chown -R searx:searx  /run/uwsgi/app/searx
#
# socket = /run/uwsgi/app/searx/socket

# Cache
cache2 = name=searxcache,items=2000,blocks=2000,blocksize=4096,bitmap=1
```
- Start it using
```
service uwsgi start   searx
```
### Morty  
It is recommended by searx developers to use morty if your searx instance faces the internet.  
```
useradd --shell /bin/bash --system --home-dir "/usr/local/morty" --comment "Privacy-respecting metasearch engine" morty
mkdir "/usr/local/morty"
chown -R "morty:morty" "/usr/local/morty"
cat > "/usr/local/morty/.go_env" <<EOF
export GOPATH=/usr/local/morty/go-apps
export PATH=$PATH:/usr/local/morty/local/go/bin:$GOPATH/bin
EOF
su - morty << EOF
echo 'source /usr/local/morty/.go_env' >> ~/.profile
mkdir /usr/local/morty/local
wget --progress=bar -O "go1.16.3.linux-amd64.tar.gz" "https://golang.org/dl/go1.16.3.linux-amd64.tar.gz"
tar -C /usr/local/morty/local -xzf "go1.16.3.linux-amd64.tar.gz"
cat ~/.profile >> ~/.bashrc
source ~/.profile
go get -v -u github.com/asciimoo/morty
EOF
```
Add a new systemd service for Morty (/etc/systemd/system/morty.service)
```
[Unit]

Description=morty
After=syslog.target
After=network.target

[Service]

Type=simple
User=morty
Group=morty
WorkingDirectory=/usr/local/morty
ExecStart=/usr/local/morty/go-apps/bin/morty -key '' -listen '127.0.0.1:3000' -timeout 5

Restart=always
Environment=USER=morty HOME=/usr/local/morty DEBUG=false

# Some distributions may not support these hardening directives.  If you cannot
# start the service due to an unknown option, comment out the ones not supported
# by your version of systemd.

ProtectSystem=full
PrivateDevices=yes
PrivateTmp=yes
NoNewPrivileges=true

[Install]

WantedBy=multi-user.target
```

### Filtron  
It is recommended by searx developers to also use filtron if your searx instance faces the internet.  
```
useradd --shell /bin/bash --system --home-dir "/usr/local/filtron" --comment "Privacy-respecting metasearch engine" filtron
mkdir "/usr/local/filtron"
chown -R "filtron:filtron" "/usr/local/filtron"
cat > "/usr/local/filtron/.go_env" <<EOF
export GOPATH=/usr/local/filtron/go-apps
export PATH=$PATH:/usr/local/filtron/local/go/bin:$GOPATH/bin
EOF
su - filtron << EOF
echo 'source /usr/local/filtron/.go_env' >> ~/.profile
mkdir /usr/local/filtron/local
wget --progress=bar -O "go1.16.3.linux-amd64.tar.gz" "https://golang.org/dl/go1.16.3.linux-amd64.tar.gz"
tar -C /usr/local/filtron/local -xzf "go1.16.3.linux-amd64.tar.gz"
cat ~/.profile >> ~/.bashrc
source ~/.profile
go get -v -u github.com/asciimoo/filtron
EOF
```

Add a new systemd service for Filtron (/etc/systemd/system/filtron.service)
```
[Unit]

Description=filtron
After=syslog.target
After=network.target

[Service]

Type=simple
User=filtron
Group=filtron
WorkingDirectory=/usr/local/filtron
ExecStart=/usr/local/filtron/go-apps/bin/filtron -api '127.0.0.1:4005' -listen '127.0.0.1:4004' -rules '/etc/filtron/rules.json' -target '127.0.0.1:8888'

Restart=always
Environment=USER=filtron HOME=/usr/local/filtron

# Some distributions may not support these hardening directives.  If you cannot
# start the service due to an unknown option, comment out the ones not supported
# by your version of systemd.

ProtectSystem=full
PrivateDevices=yes
PrivateTmp=yes
NoNewPrivileges=true

[Install]

WantedBy=multi-user.target
```

## Searx + Morty + Filtron + Nginx  

After following the steps above, an nginx configuration is needed.  
An example configuration to place in /etc/nginx/sites-enabled/ :
```
# https://example.com/
server {
    server_name example.com;
    root /usr/local/searx/searx;
    listen 80;
    location /static {
      alias /usr/local/searx/searx-src/searx/static/;
    }

    location / {
            include uwsgi_params;
            uwsgi_pass unix:/run/uwsgi/app/searx/socket;
    }

    location /searx {
      proxy_pass         http://127.0.0.1:4004/;

      proxy_set_header   Host             $host;
      proxy_set_header   Connection       $http_connection;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_set_header   X-Scheme         $scheme;
      proxy_set_header   X-Script-Name    /searx;
}

    location /morty {
      proxy_pass         http://127.0.0.1:3000/;

      proxy_set_header   Host             $host;
      proxy_set_header   Connection       $http_connection;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_set_header   X-Scheme         $scheme;
}
    location /searx/static/ {
      alias /usr/local/searx/searx-src/searx/static/;
}
}
server {
    server_name example.com;
    root /usr/local/searx/searx;
    listen 443;
    ssl on;
    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
    location /static {
      alias /usr/local/searx/searx-src/searx/static/;
    }

    location / {
            include uwsgi_params;
            uwsgi_pass unix:/run/uwsgi/app/searx/socket;
    }

    location /searx {
      proxy_pass         http://127.0.0.1:4004/;

      proxy_set_header   Host             $host;
      proxy_set_header   Connection       $http_connection;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_set_header   X-Scheme         $scheme;
      proxy_set_header   X-Script-Name    /searx;
}

    location /morty {
      proxy_pass         http://127.0.0.1:3000/;

      proxy_set_header   Host             $host;
      proxy_set_header   Connection       $http_connection;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_set_header   X-Scheme         $scheme;
}
    location /searx/static/ {
      alias /usr/local/searx/searx-src/searx/static/;
}
```
