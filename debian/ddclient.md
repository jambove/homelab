# DDclient  
Written 28-09-2021  

DDclient is a client for updating dynamic DNS entries. It is useful to set up in case the public IP address of a device is dynamic, and changes frequently.

See official website at: https://ddclient.net/

## Reference  
Please refer to the official documentation in case of issues, or if this guide is out of date:    
https://ddclient.net/#configuration   
https://ddclient.net/#documentation   

## Setup
- Install using
```
apt install ddclient
```
- Configuration file is /etc/ddclient.conf
- Here is an example configuration in case of https://www.namecheap.com
```
daemon=300
ssl=yes
use=web
web=dynamicdns.park-your-domain.com/getip
protocol=namecheap
server=dynamicdns.park-your-domain.com

# First Domain
login=example.com
password=passwordfromthewebsite
@, www.example.com, jitsi.example.com, searx.example.com

```
- In case of namecheap, the password is obtained through Domain list > Manage domainname > Advanced DNS tab > Dynamic DNS > Status  

- Restart using systemctl
```
systemctl restart ddclient
```




