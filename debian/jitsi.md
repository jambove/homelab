# Jitsi  
Written 25-04-2021  

Jitsi is a libre software for video conferencing that can be self-hosted. I prefer using it compared to other services due to the properties mentioned as well as ease of use for users: no need for additional clients, just a web browser with WebRTC.  

See official website at: https://jitsi.org/ and demo it at https://meet.jit.si/  

## Reference  
Please refer to the official documentation in case of issues, or if this guide is out of date:  
https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart  
https://jitsi.github.io/handbook/docs/devops-guide/secure-domain  

## Manual installation:
All the commands below are ran on Debian 10 and requires root privileges  
```
apt install gnupg apt-transport-https ufw
curl https://download.jitsi.org/jitsi-key.gpg.key | gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
apt update
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 4443/tcp
ufw allow 10000/udp
ufw allow 22/tcp
ufw allow 3478/udp
ufw allow 5349/tcp
ufw enable
```
- Port forward if behind NAT, in this case: Yes  
- Additionally if behind NAT, add the following lines to /etc/jitsi/videobridge/sip-communicator.properties :  
```
org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=<Local.IP.Address>
org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<Public.IP.Address>
```
- And comment the existing org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES
- Continue (if NAT or no NAT):  
```
apt install nginx
apt install jitsi-meet
```
- Create certificate   
```
/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```
### Authentication:  
- Create user and pass to avoid unwanted use of server by third parties  
```
sed -i 's/anonymous/internal_hashed/g' /etc/jitsi/meet/[your-hostname]-config.js
```
- Add the following lines below after the first virtualhost bloc in the file /etc/jitsi/meet/[your-hostname]-config.js:  
```
VirtualHost "guest.[your-hostname]"
    authentication = "anonymous"
    c2s_require_encryption = false
 ```
 - Add the following lines to /etc/jitsi/meet/[your-hostname]-config.js:  
 ```
var config = {
    hosts: {
            domain: '[your-hostname]',
            anonymousdomain: 'guest.[your-hostname]',
            ...
        },
        ...
}
```
 - Add the following lines to /etc/jitsi/jicofo/sip-communicator.properties:  
```
org.jitsi.jicofo.auth.URL=XMPP:[your-hostname]
```
 - Reboot services
```
systemctl restart prosody
systemctl restart jicofo
systemctl restart jitsi-videobridge2
```

### Enabling recording  
Using jibri: https://github.com/jitsi/jibri  
