# SSH SOCKS5 proxy, SSH reverse proxy, and SSH jumphost 
Written 28-09-2021

SSH has many functions. One of which is using the SSH tunnel as a SOCKS5 proxy, tunneling your local machine's network traffic(on some applications) through a target device, similar to a VPN. SSH can also be used as a reverse proxy, so if the target device is behind a NAT or has a dynamic public IP, it can still be reached through a remote device. Additionally, a target machine can be reached through another device called a jumphost, if both are on the same network and have the local machine's public SSH keys.

## Setup
Make sure all the remote machines have an SSH server installed and running
```
apt install openssh-server ssh
systemctl start ssh
```

### Local machine
- Generate SSH keypair if you do not have one, remember to set a good password!
```
ssh-keygen -t ed25519
```
- Copy SSH public key to the target machine
```
ssh-copy-id user@targetmachineiporhost
```
- Configure SSH hostnames in .ssh/config
```
Host targetmachinename
     HostName targetmachineiporhost
     User user
     Port 26874
     IdentityFile ~/.ssh/id_ed25519

```
- Now try ssh using new SSH keys and config
```
ssh targetmachinename
```

### Target machine
- Configuration file is /etc/ssh/sshd_config
- Check the following lines for security:
```
Port 26784
PasswordAuthentication no
PermitRootLogin no
ChallengeResponseAuthentication no
UsePAM no
```
- Note: Pubkey based access is not necessarily more secure, since a weak key or a weak password on the key may compromise the target device. Additionally, moving the SSH port does not make it more secure, it simply reduces the amount of remote scannings done by bots, keeping logs a bit cleaner. 
- Reload ssh using systemctl
```
systemctl reload ssh
```

## SOCKS5 proxy
### Local machine
- Open a terminal and run (12015 is local port)
```
ssh -D 12015 -q -C -N targetmachinename
```
- Example usage: In Firefox go to  about:preferences, search "proxy", and click settings under Network settings Configure how "Firefox connects to the internet." Select manual proxy configration, select SOCKS v5, and in the line of SOCKS host, write localhost with the port 12015.
- Now the HTTP and HTTPS traffic you browse through in Firefox while the SSH tunnel is open goes through the target device.
 

## Reverse proxy
### Remote machine
- If the target machine is behind NAT or has dynamic ip, it may create an SSH tunnel to a remote machine that has a static public IP address. After this is done, the local machine may SSH into the target machine through the existing tunnel that is connected to the remote machine.
- The public SSH key of the target machine has to be placed in /home/sshtunnel/authorized_keys (create the folders and file required)
- Add the following to the end of /etc/ssh/sshd_config
```
Port 36562
Match User sshtunnel
       GatewayPorts clientspecified 
       AllowTcpForwarding yes
```
- Reload ssh using systemctl
```
systemctl reload ssh
```

### Target machine

``` 
ssh -fN -R :16259:localhost:26784 sshtunnel@remotemachineiporhost -p 36562
``` 
- To automatically ssh even after reboot install autossh (apt install autossh)  
- Make a new systemd service by editing /etc/systemd/system/autossh.service
```
[Unit] 
Description=AutoSSH to 'remotemachine'
Requires=systemd-networkd-wait-online.service
After=systemd-networkd-wait-online.service 
[Service]
ExecStart=/usr/bin/autossh -N -M 0 -o "PubkeyAuthentication=yes" -o "StrictHostKeyChecking=false" -o "PasswordAuthentication=no" -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" -R :16259:localhost:26784 sshtunnel@remotemachineiporhost -p 36562
User=pi
[Install]
WantedBy=multi-user.target
```
- Enable and start it using systemctl
```
systemctl enable autossh
systemctl start autossh
```


### Local machine

- Connect to the target machine through the remote machine using
```
ssh user@remotemachineiporhost -p 16259
```
- or add to the .ssh/config file
```
Host targetmachinename
     HostName remotemachineiporhost
     User user
     Port 16259
     IdentityFile ~/.ssh/id_ed25519

```
- and use "ssh targetmachinename"


## Proxy through jumphost machine
- It is also possible to port forward to a jumphost machine while the target machine is not port forwarded, and SSH into the target machine through the jumphost. It is needed to place your SSH public keys on both devices.
- Add the following to your .ssh/config
```
Host jumphostmachine
     HostName jumphostmachineiporhost
     User user2
     Port 21456
     IdentityFile ~/.ssh/id_ed25519
     
Host targetmachinename
     HostName remotemachineiporhost
     User user
     Port 26874
     ProxyCommand ssh jumphostmachine nc %h %p
     IdentityFile ~/.ssh/id_ed25519
```
- This way when you run "ssh targetmachinename" the local machine first uses SSH to connect to the jumphost, then to the target by having the local machine's public keys in both. 
